package com.zmy.test;

import com.zmy.pojo.hello;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test1 {
    public static void main(String[] args) {
        //获取spring 的上下文对象
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        hello hello = (hello)context.getBean("hello");
        System.out.println(hello.toString());
    }
}
